package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.MalformedURLException;
import java.util.Scanner;

public class swing {
    Scanner in = new Scanner(System.in);
    JFrame jFrame = new JFrame(); //Подключаем класс JFrame
    JPanel jPanel = new JPanel(); //Подключаем класс JPanel

    //1
    JButton jButton1 = new JButton("OFF"); //Создание кнопки
    JButton jButton2 = new JButton("C"); //Создание кнопки
    JButton jButton3 = new JButton("C/A"); //Создание кнопки
    JButton jButton4 = new JButton("÷"); //Создание кнопки
    //2
    JButton jButton5 = new JButton("7"); //Создание кнопки
    JButton jButton6 = new JButton("8"); //Создание кнопки
    JButton jButton7 = new JButton("9"); //Создание кнопки
    JButton jButton8 = new JButton("x"); //Создание кнопки
    //3
    JButton jButton9 = new JButton("4"); //Создание кнопки
    JButton jButton10 = new JButton("5"); //Создание кнопки
    JButton jButton11 = new JButton("6"); //Создание кнопки
    JButton jButton12 = new JButton("-"); //Создание кнопки
    //4
    JButton jButton13 = new JButton("1"); //Создание кнопки
    JButton jButton14 = new JButton("2"); //Создание кнопки
    JButton jButton15 = new JButton("3"); //Создание кнопки
    //5
    JButton jButton17 = new JButton("0"); //Создание кнопки
    JButton jButton18 = new JButton("."); //Создание кнопки
    JButton jButton19 = new JButton("="); //Создание кнопки
    JButton jButton20 = new JButton("+"); //Создание кнопки

    Font font = new Font("", Font.ROMAN_BASELINE, 24); //Добавление шрифта
    Font font1 = new Font("", Font.ROMAN_BASELINE, 8); //Добавление шрифта

    void getFrame() {
        jPanel.setLayout(null); //Стандартное позиционирование
        jFrame.setTitle("Calculator"); //Название программы
        jFrame.getDefaultCloseOperation(); //Возможность закрытия приложения
        jFrame.setVisible(true); //Отображение программы
        jFrame.add(jPanel); //Добавление в флейм панели
        Toolkit toolkit = Toolkit.getDefaultToolkit(); //Подключаем класс Toolkit
        Dimension dimension = toolkit.getScreenSize(); //Получаем разрешение нашего экрана от Toolkit
        jFrame.setBounds(dimension.width / 2 - 200, dimension.height / 2 - 200, 320, 400); //Установка высоты, ширины, отступа от угла экрана

        //Поле ввода
        JTextField titleText = new JTextField(23); //Поле ввода
        titleText.setBounds(20, 20, 280, 50);
        titleText.setFont(font); //Присвоить шрифт
        titleText.setText("0");
        titleText.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (((c < '0') || (c > '9'))) {
                    e.consume();  // игнорим введенные буквы и пробел
                }
            }
        });
        jPanel.add(titleText);

        //Кнопка
        jButton1.setBounds(25, 100, 50, 30);
        jButton1.setFont(font1);
        jPanel.add(jButton1); //Добавление к панели кнопки
        jButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //закрытие
            }
        });

        jButton3.setBounds(98, 100, 50, 30);
        jButton3.setFont(font1);
        jPanel.add(jButton3); //Добавление к панели кнопки
        jButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    return;
                } else {
                    titleText.setText("0");
                }
            }
        });

        jButton2.setBounds(173, 100, 50, 30);
        jButton2.setFont(font1);
        jPanel.add(jButton2); //Добавление к панели кнопки
        jButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    return;
                } else {
                    String str;
                    str = titleText.getText();
                    str = str.substring(0, str.length() - 1);
                    if (titleText.getText().length() != 1) {
                        titleText.setText(str);
                    } else {
                        titleText.setText("0");
                    }

                }
            }
        });

        jButton4.setBounds(245, 100, 50, 30);
        jPanel.add(jButton4); //Добавление к панели кнопки
        jButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0") || titleText.getText().equals("")) {
                    return;
                } else {
                    int indexJava = titleText.getText().indexOf("÷");
                    int indexJava1 = titleText.getText().indexOf("-");
                    int indexJava2 = titleText.getText().indexOf("х");
                    int indexJava3 = titleText.getText().indexOf("+");
                    int indexJava4 = titleText.getText().indexOf(".");
                    if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                        titleText.setText(titleText.getText() + "÷");
                    }
                }
            }
        });

        //Кнопка2
        jButton5.setBounds(25, 150, 50, 30);
        jPanel.add(jButton5); //Добавление к панели кнопки
        jButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("7");
                } else {
                    titleText.setText(titleText.getText() + "7");
                }
            }
        });

        jButton6.setBounds(98, 150, 50, 30);
        jPanel.add(jButton6); //Добавление к панели кнопки
        jButton6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("8");
                } else {
                    titleText.setText(titleText.getText() + "8");
                }
            }
        });

        jButton7.setBounds(173, 150, 50, 30);
        jPanel.add(jButton7); //Добавление к панели кнопки
        jButton7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("9");
                } else {
                    titleText.setText(titleText.getText() + "9");
                }
            }
        });

        jButton8.setBounds(245, 150, 50, 30);
        jPanel.add(jButton8); //Добавление к панели кнопки
        jButton8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0") || titleText.getText().equals("")) {
                    return;
                } else {
                    int indexJava = titleText.getText().indexOf("÷");
                    int indexJava1 = titleText.getText().indexOf("-");
                    int indexJava2 = titleText.getText().indexOf("х");
                    int indexJava3 = titleText.getText().indexOf("+");
                    int indexJava4 = titleText.getText().indexOf(".");
                    if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                        titleText.setText(titleText.getText() + "х");
                    }
                }
            }
        });

        //Кнопка3
        jButton9.setBounds(25, 200, 50, 30);
        jPanel.add(jButton9); //Добавление к панели кнопки
        jButton9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("4");
                } else {
                    titleText.setText(titleText.getText() + "4");
                }
            }
        });

        jButton10.setBounds(98, 200, 50, 30);
        jPanel.add(jButton10); //Добавление к панели кнопки
        jButton10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("5");
                } else {
                    titleText.setText(titleText.getText() + "5");
                }
            }
        });

        jButton11.setBounds(173, 200, 50, 30);
        jPanel.add(jButton11); //Добавление к панели кнопки
        jButton11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("6");
                } else {
                    titleText.setText(titleText.getText() + "6");
                }
            }
        });

        jButton12.setBounds(245, 200, 50, 30);
        jPanel.add(jButton12); //Добавление к панели кнопки
        jButton12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0") || titleText.getText().equals("")) {
                    return;
                } else {
                    int indexJava = titleText.getText().indexOf("÷");
                    int indexJava1 = titleText.getText().indexOf("-");
                    int indexJava2 = titleText.getText().indexOf("х");
                    int indexJava3 = titleText.getText().indexOf("+");
                    int indexJava4 = titleText.getText().indexOf(".");
                    if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                        titleText.setText(titleText.getText() + "-");
                    }
                }
            }
        });

        //Кнопка4
        jButton13.setBounds(25, 250, 50, 30);
        jPanel.add(jButton13); //Добавление к панели кнопки
        jButton13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("1");
                } else {
                    titleText.setText(titleText.getText() + "1");
                }
            }
        });

        jButton14.setBounds(98, 250, 50, 30);
        jPanel.add(jButton14); //Добавление к панели кнопки
        jButton14.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("2");
                } else {
                    titleText.setText(titleText.getText() + "2");
                }
            }
        });

        jButton15.setBounds(173, 250, 50, 30);
        jPanel.add(jButton15); //Добавление к панели кнопки
        jButton15.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    titleText.setText("3");
                } else {
                    titleText.setText(titleText.getText() + "3");
                }
            }
        });

        jButton20.setBounds(245, 250, 50, 80);
        jPanel.add(jButton20); //Добавление к панели кнопки
        jButton20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0") || titleText.getText().equals("")) {
                    return;
                } else {
                    int indexJava = titleText.getText().indexOf("÷");
                    int indexJava1 = titleText.getText().indexOf("-");
                    int indexJava2 = titleText.getText().indexOf("х");
                    int indexJava3 = titleText.getText().indexOf("+");
                    int indexJava4 = titleText.getText().indexOf(".");
                    if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                        titleText.setText(titleText.getText() + "+");
                    }
                }
            }
        });

        //Кнопка5
        jButton17.setBounds(25, 300, 50, 30);
        jPanel.add(jButton17); //Добавление к панели кнопки
        jButton17.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    return;
                } else {
                    titleText.setText(titleText.getText() + "0");
                }
            }
        });


        jButton18.setBounds(98, 300, 50, 30);
        jPanel.add(jButton18); //Добавление к панели кнопки
        jButton18.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0") || titleText.getText().equals("")) {
                    titleText.setText("0.");
                } else {
                    int indexJava = titleText.getText().indexOf("÷");
                    int indexJava1 = titleText.getText().indexOf("-");
                    int indexJava2 = titleText.getText().indexOf("х");
                    int indexJava3 = titleText.getText().indexOf("+");
                    int indexJava4 = titleText.getText().indexOf(".");
                    if (indexJava == -1 && indexJava3 == -1 && indexJava1 == -1 && indexJava2 == -1 && indexJava4 == -1) {
                        titleText.setText(titleText.getText() + ".");
                    }
                }
            }
        });

        jButton19.setBounds(173, 300, 50, 30);
        jPanel.add(jButton19); //Добавление к панели кнопки
        jButton19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (titleText.getText().equals("0")) {
                    return;
                } else {
                    boolean on = true;
                    String num1 = "";
                    int n1 = 0;
                    int indexJava1 = titleText.getText().indexOf("х");
                    if (indexJava1 != -1) {
                        for (String retval : titleText.getText().split("х")) {
                            String[] in = new String[2];

                            if (on) {
                                in[0] = retval;
                                on = false;
                                num1 = in[0];
                                n1 = Integer.parseInt(num1);
                                continue;
                            }
                            in[1] = retval;
                            String num2 = in[1];
                            int n2 = Integer.parseInt(num2);
                            int resulting = n1 * n2;
                            String resultConvert = Integer.toString(resulting);
                            titleText.setText(resultConvert);
                        }
                    }
                    int indexJava2 = titleText.getText().indexOf("-");
                    if (indexJava2 != -1) {
                        for (String retval : titleText.getText().split("-")) {
                            String[] in = new String[2];

                            if (on) {
                                in[0] = retval;
                                on = false;
                                num1 = in[0];
                                n1 = Integer.parseInt(num1);
                                continue;
                            }
                            in[1] = retval;
                            String num2 = in[1];
                            int n2 = Integer.parseInt(num2);
                            int resulting = n1 - n2;
                            String resultConvert = Integer.toString(resulting);
                            titleText.setText(resultConvert);
                        }
                    }
                    int indexJava3 = titleText.getText().indexOf("÷");
                    if (indexJava3 != -1) {
                        for (String retval : titleText.getText().split("÷")) {
                            String[] in = new String[2];

                            if (on) {
                                in[0] = retval;
                                on = false;
                                num1 = in[0];
                                n1 = Integer.parseInt(num1);
                                continue;
                            }
                            in[1] = retval;
                            String num2 = in[1];
                            int n2 = Integer.parseInt(num2);
                            int resulting = n1 / n2;
                            String resultConvert = Integer.toString(resulting);
                            titleText.setText(resultConvert);
                        }
                    }
                    int indexJava4 = titleText.getText().indexOf("+");
                    if (indexJava4 != -1) {
                        for (String retval : titleText.getText().split("\\+")) {
                            String[] in = new String[2];

                            if (on) {
                                in[0] = retval;
                                on = false;
                                num1 = in[0];
                                n1 = Integer.parseInt(num1);
                                continue;
                            }
                            in[1] = retval;
                            String num2 = in[1];
                            int n2 = Integer.parseInt(num2);
                            int resulting = n1 + n2;
                            String resultConvert = Integer.toString(resulting);
                            titleText.setText(resultConvert);
                        }
                    }
                }
            }
        });
    }
}


